#ifndef ALIGNMENT_TTYALIGNMENT_TTYALIGNMENT_H
#define ALIGNMENT_TTYALIGNMENT_TTYALIGNMENT_H

// STL
#include <map>
#include <string>
#include <vector>
#include <list>
#include <utility>
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
// ROOT
#include "TCanvas.h"
#include "TFitResult.h"
#include "TString.h"
#include "TCut.h"
#include "TH1I.h"
#include "TH2I.h"
#include "TProfile.h"
#include "THStack.h"
#include "TPaveStats.h"
#include "TLegend.h"
#include "TF1.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TKey.h"

#include "TGraphErrors.h"
// RooFit
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooProdPdf.h"
#include "RooAddPdf.h"
#include "RooGaussian.h"
#include "RooEfficiency.h"
#include "RooEffProd.h"
#include "RooUnblindUniform.h"
#include "RooChebychev.h"
#include "RooConstVar.h"
#include "RooExponential.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooStats/SPlot.h"
#include "RooCategory.h"
#include "RooAbsBinning.h"
#include "RooBinning.h"
#include "RooEffProd.h"
#include "RooGaussModel.h"
#include "RooTruthModel.h"
#include "RooDecay.h"
#include "RooSimultaneous.h"
#include "RooHist.h"
#include "RooCurve.h"
#include "RooCustomizer.h"
#include "RooGenericPdf.h"
#include "RooTrace.h"
#include "RooAbsDataStore.h"

#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>

#include "TVerticalAlignment/ST2DPlot.h"
#include "TVerticalAlignment/Parser.h"
#include "TVerticalAlignment/TVerticalAlignment.h"

using namespace std;
using namespace RooFit;

namespace Alignment
{
  namespace TVerticalAlignment
  {

    class TTYAlignMagOff
    {
      public:
        // Constructors
        TTYAlignMagOff( TString filename, TString dbfilename, TString txtfiledir, bool constraint=false, bool saveplots=false);
        // Destructor
        ~TTYAlignMagOff() {}
         
        // Methods
        void fit_efficiency();
	void plots();
	void glimpse_data();
	void debug_verbose();

      private:
        TVerticalAlignment* m_va;
        Param m_param;
        TString m_filename;
        TString m_dbfilename;
        TString m_outDirectory;
        bool m_constraint;
        bool m_saveplots;

	bool m_glimpse = false;
	bool m_verbose = false;

        STNames* m_Names;
        ofstream m_YPosFitFile;
        ofstream m_LengthFitFile;
	TString  m_outputname;

        void FitfromROOTFile();
        void GetMeanFromHisto(TString SectorName);
        RooDataSet* GetRooDataSetFromTH1(TH1F* histo, TH1F* histo_exp, RooRealVar y, RooRealVar weight, RooCategory cat_lr, RooCategory cat_eff, bool add_lr=true, double offset=0);
    };
    
  }// namespace TVerticalAlignment
}// namespace Alignment

#endif
