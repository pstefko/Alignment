/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFitter.h
 *
 *    Description:  (1) create the pi0 mass histogram; 
 *                  (2) fit the histogram; 
 *                  (3) save and return the results
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:32:16 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef CALIBRATION_PI0CALIBRATION_PI0MASSFITTER_H
#define CALIBRATION_PI0CALIBRATION_PI0MASSFITTER_H 1

#include <iostream>
#include <string>
#include <utility>

#include "TH1.h"

namespace Calibration
{
    namespace Pi0Calibration 
    {
	class Pi0MassFitter
	{
	    public:
		Pi0MassFitter(TH1* hist, TH1* hist_bg, double mpi0, double sigma );
		virtual ~Pi0MassFitter(){}

                void   chi2fit( const std::string& outputdir, const std::string& outfilename, bool verbose );
                void   likelihoodfit( const std::string& outputdir, const std::string& outfilename, bool verbose );
		std::pair<double,double> getMean() {return m_mean; }
		std::pair<double,double> getSigma() {return m_sigma; }
                int    getStatus() {return m_status; }

	    private:

		TH1* m_hist;
		TH1* m_hist_bg;
		double m_mpi0;
		double m_sigma0;

		std::pair<double,double> m_mean;
		std::pair<double,double> m_sigma;
                int    m_status;
	};
    }//namespace Pi0Calibration
}//namespace Calibration

#endif
